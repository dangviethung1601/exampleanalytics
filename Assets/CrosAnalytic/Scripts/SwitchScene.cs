using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CrosGame
{
    public class SwitchScene : MonoBehaviour
    {
        public string sceneToLoad;

        public void LoadScene()
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }
}
