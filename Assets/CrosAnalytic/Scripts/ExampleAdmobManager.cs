﻿//#define use_admob

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Object = System.Object;
using GoogleMobileAds.Common;
using Firebase;
using GoogleMobileAds.Api;
using CrosGame;

// ReSharper disable InconsistentNaming
namespace CrosGame
{
    public class ExampleAdmobManager : MonoBehaviour
    {
        public bool testAds;


#if UNITY_ANDROID
        protected virtual string BannerId => testAds ? TestBannerId : bannerId;
        protected virtual string InterstitialId => testAds ? TestInterstitialId : interstitialId;
        protected virtual string VideoId => testAds ? TestVideoId : videoId;
        protected virtual string OpenId => testAds ? TestOpenId : openId;


        protected const string TestBannerId = "ca-app-pub-3940256099942544/6300978111";
        protected const string TestInterstitialId = "ca-app-pub-3940256099942544/1033173712";
        protected const string TestVideoId = "ca-app-pub-3940256099942544/5224354917";
        protected const string TestOpenId = "ca-app-pub-3940256099942544/3419835294";
#else
//#if UNITY_IOS*
    private string BannerId => testAds ? TestBannerId : bannerId_ios;
    private string InterstitialId => testAds ? TestInterstitialId : interstitialId_ios;
    private string VideoId => testAds ? TestVideoId : videoId_ios;
    private string OpenId => testAds ? TestOpenId : openId_ios;

    private const string TestBannerId = "ca-app-pub-3940256099942544/2934735716";
    private const string TestInterstitialId = "ca-app-pub-3940256099942544/4411468910";
    private const string TestVideoId = "ca-app-pub-3940256099942544/1712485313";
    private const string TestOpenId = "ca-app-pub-3940256099942544/5662855259";
#endif


        [Header("Android")] public string bannerId;
        public string interstitialId;
        public string videoId;
        public string openId;

        [Header("IOS")] public string bannerId_ios;
        public string interstitialId_ios;
        public string videoId_ios;
        public string openId_ios;

        private RewardedAd _rewardedAd;
        private InterstitialAd _interstitial;
        private BannerView _bannerView;
        private AppOpenAd _appOpenAds;
        private bool _initialized;

        private string bannerAdapter;
        private string interstitialAdapter;
        private string rewardedAdapter;
        private string openAdsAdapter;

        private float _timeShowInterstitial = 0f;
        private float _timeShowRewardsAds = 0f;


        #region Example
        public void ExampleRequestBanner()
        {
            RequestBanner();
        }
        #endregion

        private IEnumerator IeRequestBanner(float time)
        {
            yield return new WaitForSeconds(time);
            RequestBanner();
        }

        private IEnumerator IeRequestInters(float time)
        {
            yield return new WaitForSeconds(time);
            RequestInterstitial();
        }

        private IEnumerator IeRequestReward(float time)
        {
            yield return new WaitForSeconds(time);
            RequestRewardedAd();
        }

        private IEnumerator IeRequestOpenAds(float time)
        {
            yield return new WaitForSeconds(time);
            RequestOpenAds();
        }

        public bool IsInitialized()
        {
            return _initialized;
        }

        public bool BannerIsShowing()
        {
            return _bannerState == AdsState.Loaded;
        }

        public bool IntersIsShowing()
        {
            return _intersState == AdsState.Showing;
        }

        public bool VideoIsShowing()
        {
            return _videoState == AdsState.Showing;
        }

        public void DestroyBanner()
        {
            _bannerState = AdsState.NotStart;
            if (_bannerView == null)
                return;
            _bannerView.Destroy();
            _bannerView = null;
        }

        private void Start()
        {
            StartCoroutine(waitFor());
        }


        IEnumerator waitFor(float timeWaitMax = 5)
        {
            float t = timeWaitMax;
            while (t > 0 && CrosAnalyticTracker.FirebaseReady)
            {
                yield return null;
                t -= Time.deltaTime;
            }

            SetupAdmob();
        }

        private void SetupAdmob()
        {
#if UNITY_IOS
        MobileAds.SetiOSAppPauseOnBackground(true);
#endif

            RequestConfiguration requestConfiguration =
                new RequestConfiguration.Builder().build();
            MobileAds.SetRequestConfiguration(requestConfiguration);
            MobileAds.Initialize(HandleInitCompleteAction);
        }

        private enum AdsState
        {
            NotStart,
            Requesting,
            Failed,
            Loaded,
            Showing,
            Closed
        }

        private AdsState _bannerState = AdsState.NotStart;
        private AdsState _intersState = AdsState.NotStart;
        private AdsState _openAdsState = AdsState.NotStart;
        private AdsState _videoState = AdsState.NotStart;

        public void CheckShowBanner()
        {
            switch (_bannerState)
            {
                case AdsState.Requesting:
                    break;
                case AdsState.Failed:
                    RequestBanner();
                    break;
                case AdsState.Loaded:
                    break;
                case AdsState.Showing:
                    break;
                case AdsState.Closed:
                    RequestBanner();
                    break;
                case AdsState.NotStart:
                    RequestBanner();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void HandleInitCompleteAction(InitializationStatus initStatus)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _initialized = true;
                StartCoroutine(IeRequestBanner(0.5f));
                StartCoroutine(IeRequestInters(1));
                StartCoroutine(IeRequestOpenAds(2));
                StartCoroutine(IeRequestReward(3));
            });
        }

        public void RequestNewAds()
        {
            _bannerView?.Destroy();
            _bannerView = null;
            _bannerState = AdsState.NotStart;

            _interstitial?.Destroy();
            _interstitial = null;
            _intersState = AdsState.NotStart;

            _rewardedAd?.Destroy();
            _rewardedAd = null;
            _videoState = AdsState.NotStart;


            StartCoroutine(IeRequestBanner(5));
            StartCoroutine(IeRequestInters(10));
            StartCoroutine(IeRequestOpenAds(10));
            StartCoroutine(IeRequestReward(12));
        }

        private void RequestBanner()
        {
            if (_bannerState == AdsState.Requesting)
                return;

            if (_initialized)
            {
                DestroyBanner();
                _bannerView = null;

                _adaptiveSize = AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth);
                _bannerView = new BannerView(BannerId, _adaptiveSize, AdPosition.Bottom);
                _bannerView.OnAdLoaded += HandleBannerAdLoaded;
                _bannerView.OnAdFailedToLoad += HandleBannerAdFailedToLoad;
                _bannerView.OnAdOpening += HandleBannerAdOpened;
                _bannerView.OnAdClosed += HandleBannerAdClosed;

                _bannerView.OnPaidEvent += (sender, args) =>
                {
                    HandleAdPaidEvent(sender, args, AdType.banner, bannerAdapter, "googleadmob");
                };

                // Create an empty ad request.
                AdRequest request = new AdRequest.Builder().Build();
                // Load the banner with the request.
                _bannerView.LoadAd(request);
                _bannerState = AdsState.Requesting;
            }
        }

        private void RequestInterstitial()
        {
            //        if (!Kernel.IsInternetConnection())
            //        {
            //            if (logFail) //UIDebugLog.Log("AdsInstanceAdmob: RequestInterstitial failed no Internet", LogType.Service);
            //                _intersState = AdsState.Failed;
            //            HandleOnInterstitialAdFailedToLoad(null, null);
            //            return;
            //        }

            if (_interstitial != null)
                _interstitial.Destroy();
            _interstitial = null;
            _interstitial = new InterstitialAd(InterstitialId);
            // Load the interstitial with the request.
            // Called when an ad request has successfully loaded.
            _interstitial.OnAdLoaded += HandleOnInterstitialAdLoaded;
            // Called when an ad request failed to load.
            _interstitial.OnAdFailedToLoad += HandleOnInterstitialAdFailedToLoad;
            // Called when an ad is shown.
            _interstitial.OnAdOpening += HandleOnInterstitialAdOpening;
            // Called when the ad is closed.
            _interstitial.OnAdClosed += HandleOnInterstitialAdClosed;
            _interstitial.OnAdFailedToShow += HandleOnInterstitialAdFailedToShow;
            // Called when the ad click caused the user to leave the application.
            _interstitial.OnPaidEvent += (sender, args) =>
            {
                HandleAdPaidEvent(sender, args, AdType.interstitial, interstitialAdapter, "googleadmob");
            };

            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            // Load the interstitial with the request.
            _interstitial.LoadAd(request);
        }

        private void RequestOpenAds()
        {
            _openAdsState = AdsState.Requesting;

            if (_appOpenAds != null)
                _appOpenAds.Destroy();
            _appOpenAds = null;

            AdRequest request = new AdRequest.Builder().Build();
            AppOpenAd.LoadAd(OpenId, ScreenOrientation.Portrait, request, ((appOpenAd, error) =>
            {
                if (error != null)
                {
                    // Handle the error.
                    Debug.LogErrorFormat("Failed to load the ad. (reason: {0})", error.LoadAdError.GetMessage());
                    HandleOnOpenAdFailedToLoad(error);
                }
                else
                {
                    _appOpenAds = appOpenAd;
                    Debug.Log("_appOpenAds" + _appOpenAds);
                    HandleOnOpenAdLoaded();
                }

                // App open ad is loaded.
            }));

            //        var fireBase = Kernel.Resolve<FireBaseController>();
            //        var parameters = fireBase.GetAdmobParameter(3, new LogParameter[]
            //        {
            //        },AdType.app_open).ToArray();
            //        fireBase.LogEvent("admob_ad_request",parameters);
        }

        public bool IsOpenAdsReady()
        {
            return _appOpenAds == null;
        }

        public bool ShowOpenAds()
        {
            if (IsOpenAdsReady() || _openAdsState == AdsState.Showing)
            {
                return false;
            }

            _appOpenAds.OnAdDidDismissFullScreenContent += HandleAdDidDismissFullScreenContent;
            _appOpenAds.OnAdFailedToPresentFullScreenContent += OnAdFailedToPresentFullScreenContent;
            _appOpenAds.OnAdDidPresentFullScreenContent += OnAdDidPresentFullScreenContent;
            // _appOpenAds.OnAdDidRecordImpression += OnAdDidRecordImpression;
            _appOpenAds.OnPaidEvent += (sender, args) =>
            {
                HandleAdPaidEvent(sender, args, AdType.app_open, openAdsAdapter, "googleadmob");
            };

            Debug.Log("show open ads");
            _appOpenAds.Show();
            return true;
        }

        public void RequestRewardedAd()
        {
            _rewardedAd?.Destroy();
            _rewardedAd = null;
            _rewardedAd = new RewardedAd(VideoId);
            _rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
            _rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
            _rewardedAd.OnAdClosed += HandleRewardedAdClosed;
            _rewardedAd.OnAdFailedToLoad += HandleOnRewardAdFailedToLoad;
            _rewardedAd.OnAdFailedToShow += HandleOnRewardAdFailedToShow;
            _rewardedAd.OnPaidEvent += (sender, args) =>
            {
                HandleAdPaidEvent(sender, args, AdType.rewarded_video, rewardedAdapter, "googleadmob");
            };
            _rewardedAd.OnAdOpening += HandleVideoAdOpeningEvent;
            AdRequest request = new AdRequest.Builder()
                .Build();

            _rewardedAd.LoadAd(request);
        }

        private void HandleBannerAdLoaded(object sender, EventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _bannerState = AdsState.Loaded;
                bannerAdapter = _bannerView.GetResponseInfo().GetMediationAdapterClassName();
            });
        }

        private void HandleOnInterstitialAdLoaded(object sender, EventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                retryIntersAttempt = 1;
                _intersState = AdsState.Loaded;
                interstitialAdapter = _interstitial.GetResponseInfo().GetMediationAdapterClassName();
            });
        }

        private void HandleRewardedAdLoaded(object sender, EventArgs e)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _retryRewardAttempt = 1;
                _videoState = AdsState.Loaded;
                rewardedAdapter = _rewardedAd.GetResponseInfo().GetMediationAdapterClassName();
            });
        }

        private void HandleAdPaidEvent(object sender, AdValueEventArgs args, AdType adType, string adapter, string platform)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                AdValue adValue = args.AdValue;
                CrosAnalyticTracker.LogFirebaseRevenue(platform, GetNetworkName(adapter),
                    adValue.Value / 1000000f, adValue.Precision.ToString(), adType.ToString(), adValue.CurrencyCode);
                CrosAnalyticTracker.LogAppsFlyerAdRevenue(GetNetworkName(adapter),
                    (double)adValue.Value / 1000000f, adType.ToString());
            });
        }

        protected static string GetNetworkName(string fullNetworkName, string defaultNetwork = "admob")
        {
            if (string.IsNullOrEmpty(fullNetworkName))
                return defaultNetwork;
            var lower = fullNetworkName.ToLower();
            if (lower.Contains("admob"))
                return "admob";
            if (lower.Contains("max"))
                return "applovinmax";
            if (lower.Contains("fyber"))
                return "fyber";
            if (lower.Contains("appodeal"))
                return "appodeal";
            if (lower.Contains("inmobi"))
                return "inmobi";
            if (lower.Contains("vungle"))
                return "vungle";
            if (lower.Contains("admost"))
                return "admost";
            if (lower.Contains("topon"))
                return "topon";
            if (lower.Contains("tradplus"))
                return "tradplus";
            if (lower.Contains("chartboost"))
                return "chartboost";
            if (lower.Contains("appodeal"))
                return "appodeal";
            if (lower.Contains("google"))
                return "googleadmanager";
            if (lower.Contains("google"))
                return "googlead";
            if (lower.Contains("facebook") || lower.Contains("meta"))
                return "facebook";
            if (lower.Contains("applovin") || lower.Contains("max"))
                return "applovin";
            if (lower.Contains("ironsource"))
                return "ironsource";
            if (lower.Contains("unity"))
                return "unity";
            if (lower.Contains("mintegral"))
                return "mtg";
            return defaultNetwork;
        }

        private void HandleVideoAdOpeningEvent(object sender, EventArgs e)
        {
            _timeShowRewardsAds = Time.realtimeSinceStartup;
            MobileAdsEventExecutor.ExecuteInUpdate(() => { _videoState = AdsState.Showing; });
        }


        private AdSize _adaptiveSize;


        private float _lastTimeFocus;

        private void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus)
            {
                if (Time.realtimeSinceStartup - _lastTimeFocus > 30 * 60)
                {
                    StartCoroutine(IeRequestBanner(1));
                    StartCoroutine(IeRequestInters(10));
                    StartCoroutine(IeRequestReward(12));
                }

                _lastTimeFocus = Time.realtimeSinceStartup;
            }
        }


        int _retryBannerAttempt;

        private void HandleBannerAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _bannerState = AdsState.Failed;
                StartCoroutine(InvokeBanner(false));

                _retryBannerAttempt++;
                double retryDelay = Math.Pow(2, Mathf.Clamp(_retryBannerAttempt, 3, 7));
                _bannerView?.Destroy();
                _bannerView = null;

                StartCoroutine(IeRequestBanner((float)retryDelay));
            });
        }

        private IEnumerator InvokeBanner(bool value)
        {
            yield return new WaitForSeconds(0.1f);
        }

        private void HandleBannerAdOpened(object sender, EventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() => { StartCoroutine(InvokeBanner(false)); });
        }

        private void HandleBannerAdClosed(object sender, EventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                DestroyBanner();
                StartCoroutine(IeRequestBanner(.5f));
                StartCoroutine(InvokeBanner(false));
                StartCoroutine(waitToBannerFalse());
            });
        }

        IEnumerator waitToBannerFalse()
        {
            yield return new WaitForSeconds(5);
        }

        public void HandleAdLeftApplication(object sender, EventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() => { });
        }


        private int _retryRewardAttempt;

        private void HandleOnRewardAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _retryRewardAttempt++;
                double retryDelay = Math.Pow(2, Mathf.Clamp(_retryRewardAttempt, 3, 7));
                _rewardedAd?.Destroy();
                _rewardedAd = null;
                StartCoroutine(IeRequestReward((float)retryDelay));
            });
        }

        private void HandleOnRewardAdFailedToShow(object sender, AdErrorEventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _retryRewardAttempt++;
                double retryDelay = Math.Pow(2, Mathf.Clamp(_retryRewardAttempt, 3, 7));
                _rewardedAd?.Destroy();
                _rewardedAd = null;
                StartCoroutine(IeRequestReward((float)retryDelay));
            });
        }

        private void HandleRewardedAdClosed(object sender, EventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _videoState = AdsState.Closed;
                StartCoroutine(IeRequestReward(.5f));
                StartCoroutine(waitToRewardFalse());

                _timeShowRewardsAds = -1000;
            });
        }

        IEnumerator waitToRewardFalse()
        {
            yield return new WaitForSeconds(3);
        }

        private void HandleUserEarnedReward(object sender, Reward e)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() => { });
        }


        public void ShowRewardAds()
        {
            if (IsVideoAdsReady())
            {
                _rewardedAd.Show();
            }
        }

        public bool IsVideoAdsReady()
        {
            return _rewardedAd != null && _rewardedAd.IsLoaded();
        }

        public bool IsRealInterstitialReady()
        {
            return _interstitial != null && _interstitial.IsLoaded();
        }

        public void ShowInterstitial()
        {
            if (IsRealInterstitialReady())
            {
                _interstitial.Show();
            }
        }

        int retryIntersAttempt;


        private void HandleOnInterstitialAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                retryIntersAttempt++;
                double retryDelay = Math.Pow(2, Mathf.Clamp(retryIntersAttempt, 3, 8));
                _interstitial?.Destroy();
                _interstitial = null;
                StartCoroutine(IeRequestInters((float)retryDelay));
            });
        }

        private void HandleOnInterstitialAdFailedToShow(object sender, AdErrorEventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _interstitial?.Destroy();
                _interstitial = null;
                _intersState = AdsState.Closed;
                StartCoroutine(IeRequestInters(.5f));
            });
        }

        private void HandleOnInterstitialAdOpening(object sender, EventArgs args)
        {
            _timeShowInterstitial = Time.realtimeSinceStartup;
            MobileAdsEventExecutor.ExecuteInUpdate(() => { _intersState = AdsState.Showing; });
        }

        private void HandleOnInterstitialAdClosed(object sender, EventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _intersState = AdsState.Closed;
                StartCoroutine(IeRequestInters(.5f));
                StartCoroutine(waitToInterstitialFalse());
                _timeShowInterstitial = -1000;
            });
        }

        IEnumerator waitToInterstitialFalse()
        {
            yield return new WaitForSeconds(3);
        }

        // open ads
        private int retryOpenAttempt;

        private void HandleOnOpenAdFailedToLoad(AdFailedToLoadEventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                retryOpenAttempt++;
                double retryDelay = Math.Pow(2, Mathf.Clamp(retryOpenAttempt, 3, 8));
                _appOpenAds?.Destroy();
                _appOpenAds = null;
                StartCoroutine(IeRequestOpenAds((float)retryDelay));
            });
        }

        private void OnAdFailedToPresentFullScreenContent(object sender, AdErrorEventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _appOpenAds?.Destroy();
                _appOpenAds = null;
                _openAdsState = AdsState.Closed;
                StartCoroutine(IeRequestOpenAds(.5f));
            });
        }

        private void OnAdDidPresentFullScreenContent(object sender, EventArgs args)
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() => { });
        }

        private void HandleAdDidDismissFullScreenContent(object sender, EventArgs args)
        {
            Debug.Log("Closed app open ad");
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                _openAdsState = AdsState.Closed;
                StartCoroutine(IeRequestOpenAds(.5f));
                StartCoroutine(waitToOpenFalse());
            });
        }

        private void HandleOnOpenAdLoaded()
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                retryOpenAttempt = 1;
                _openAdsState = AdsState.Loaded;
            });
        }


        IEnumerator waitToOpenFalse()
        {
            yield return new WaitForSeconds(3);
        }

        public void HandleOnOpenAdLeavingApplication(object sender, EventArgs args)
        {
        }
    }
}