﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[InitializeOnLoad]
public class EditorInit
{
    static EditorInit()
    {
        EditorApplication.playModeStateChanged += PlayModeStateChange;
    }

    private static void PlayModeStateChange(PlayModeStateChange playModeState)
    {
        if (playModeState == UnityEditor.PlayModeStateChange.ExitingPlayMode)
        {
            EditorSceneManager.playModeStartScene = null;
        }
    }

    [MenuItem("Tools/Open Start Scene")]
    public static void OpenStartScene()
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.OpenScene("Assets/CrosAnalytic/Scenes/StartScene.unity");
        }
    }

    [MenuItem("Tools/Play Start Scene")]
    public static void PlayStartScene()
    {
        EditorSceneManager.playModeStartScene = AssetDatabase.LoadAssetAtPath<SceneAsset>("Assets/CrosAnalytic/Scenes/StartScene.unity");
        EditorApplication.EnterPlaymode();
    }


    [MenuItem("Tools/Open Scene Admob")]
    public static void OpenSceneExampleAdmob()
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.OpenScene("Assets/CrosAnalytic/Scenes/AdmobExample.unity");
        }
    }

    [MenuItem("Tools/Play Scene Admob")]
    public static void PlaySceneExampleAdmob()
    {
        EditorSceneManager.playModeStartScene = AssetDatabase.LoadAssetAtPath<SceneAsset>("Assets/CrosAnalytic/Scenes/AdmobExample.unity");
        EditorApplication.EnterPlaymode();
    }

    [MenuItem("Tools/Open Scene Tracking")]
    public static void OpenSceneExampleTracking()
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.OpenScene("Assets/CrosAnalytic/Scenes/LogEventTrackingExample.unity");
        }
    }

    [MenuItem("Tools/Play Scene Tracking")]
    public static void PlaySceneExampleTracking()
    {
        EditorSceneManager.playModeStartScene = AssetDatabase.LoadAssetAtPath<SceneAsset>("Assets/CrosAnalytic/Scenes/LogEventTrackingExample.unity");
        EditorApplication.EnterPlaymode();
    }

    [MenuItem("Tools/Clear All PlayerPrefs")]
    public static void ClearAllPlayerPrefs()
    {
        Debug.Log("PlayerPrefs Cleared");
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }
}