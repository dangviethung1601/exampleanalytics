using Firebase;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;
using Firebase.Analytics;
using UnityEngine;
using System;

namespace CrosGame
{
    public class CrosEventTracker : MonoBehaviour
    {

        public void LogEventScreen()
        {
            LogScreenView logScreenView = new LogScreenView();
            logScreenView.screen_class = "LogEventTrackingExample";
            logScreenView.screen_name = "LogEventTrackingExample";
            logScreenView.PostEvent();

            ScreenViewAppsflyer screenViewAppsflyer = new ScreenViewAppsflyer();
            screenViewAppsflyer.screen_class = "LogEventTrackingExample";
            screenViewAppsflyer.screen_name = "LogEventTrackingExample";
            screenViewAppsflyer.PostEvent();
        }
    }
}