using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneController : MonoBehaviour
{
    public void LoadSceneAdmob()
    {
        SceneManager.LoadScene("AdmobExample");
    }

    public void LoadSceneTracking()
    {
        SceneManager.LoadScene("LogEventTrackingExample");
    }
}
