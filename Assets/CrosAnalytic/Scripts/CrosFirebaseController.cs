using Firebase;
using Firebase.Analytics;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrosGame
{
    public class CrosFirebaseController : MonoBehaviour
    {
        public static DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
        // Start is called before the first frame update
        void Start()
        {
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
            {
                dependencyStatus = task.Result;
                Debug.Log("CheckAndFixDependenciesAsync " + dependencyStatus);
                if (dependencyStatus == DependencyStatus.Available)
                {
                    CrosAnalyticTracker.FirebaseReady = true;
                    InitializeFirebase();
                }
                else
                {
                    Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
                }
            });
        }
        private void InitializeFirebase()
        {
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
        }
    }
}